from interfaces import MXInterface


class CapConfiguration:
    cap_interface = MXInterface()
    thumb_cluster_secondary_radius = 50

    thumb_row_1_caps_height = 5.8
    thumb_row_1_caps_shrink_factor = 0.8

    thumb_row_2_caps_inner_height = 4
    thumb_row_2_caps_rounding_height = 4.8
    thumb_row_2_caps_upper_length = 8
    thumb_row_2_caps_lower_length = 10
    thumb_row_2_caps_base_height = 3.2
    thumb_row_2_caps_slant = 18


