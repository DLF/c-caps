from solid import *
from solid.utils import *

"""
“Negative” hole for MX Swiches. Use it as as
subtrahend in a difference operation.

The hole at z=0 is the level on which the MX
rim hits the plate. The hole can be further
lowered, a bigger hole having space for the
rim is above z=0. The hole can easily be lowered
by 0.5mm.

The hole stencil is centered on the xy-plane.

The inner hole is 14mm^2.
The basic cutaway around is 16mm^2.
The gap in the MX rim is 11mm long

BTW: The actual width of the switch (at rim) is likely specified as 15.6mm.
"""


class MXSwitchStamp(object):

    def __init__(
        self,
        switch_x=14.25,
        switch_y=14.3,
        switch_hat_x=16.4,
        switch_hat_y=16.4,
        clip_height=1.6,
        clip_depth=1,
        clip_width=5.5,
    ):
        self.switch_x = switch_x
        self.switch_y = switch_y
        self.switch_hat_x = switch_hat_x
        self.switch_hat_y = switch_hat_y
        self.clip_height = clip_height
        self.clip_depth = clip_depth
        self.clip_width = clip_width
        self.hat_cutaway_width = 10
        self.total_height = 19
        self.top_height = 10
        self.lower_cutaway_height = self.total_height - self.top_height - self.clip_height

    def __call__(self, *args, **kwargs):
        return(
            translate((0, 0, -self.total_height/2 + self.top_height))(
                rotate(90)(
                    # rotate around z-axis so that the base positon is like looking from the front of the keyboard
                    color(Red)(cube([self.switch_x, self.switch_y, self.total_height], center=True))
                    +
                    translate((0, 0, 0))(
                        color(Yellow)(
                            # lower cut-away
                            translate((0, 0, -(self.total_height-self.lower_cutaway_height)/2))(
                                cube(
                                    [
                                        self.switch_y + 2 * self.clip_depth,
                                        self.clip_width,
                                        self.lower_cutaway_height
                                    ],
                                    center=True
                                )
                            )
                            +
                            # upper cut-away
                            translate((0, 0, (self.total_height - self.top_height)/2))(
                                cube([self.switch_hat_x, self.switch_hat_y, self.top_height + 0.01], center=True)
                                -
                                translate((0, 0, -0.5))(
                                    cube(
                                        [
                                            self.hat_cutaway_width,
                                            self.switch_hat_y + 1,
                                            self.top_height
                                        ],
                                        center=True
                                    )
                                )
                            )
                        )
                    )
                )
            )
        )

