import os
import pathlib

from terminal_c import geometry
from terminal_c.configuration.default import Configuration as TermCConfig

import caps
from configuration import CapConfiguration

from solid import (
    scad_render_to_file,
    OpenSCADObject,
    cube,
    translate
)

_this_path = pathlib.Path(__file__).parent.resolve()


class Renderer:
    def __init__(self):
        self.outpath = pathlib.Path(os.path.join(_this_path, "../..", "scad")).resolve()
        if not os.path.exists(self.outpath):
            os.makedirs(self.outpath)

    def __call__(self, name: str, obj: OpenSCADObject):
        outfile = str(self.outpath) + "/" + name + ".scad"
        print("Render {}".format(outfile))
        scad_render_to_file(obj, outfile)


if __name__ == "__main__":
    c = CapConfiguration()
    termc_config = TermCConfig()
    pcb_board = geometry.Board(termc_config)
    r = Renderer()

    finger_cap = caps.FingerCap(
        c,
        elongation=0,
        tilt=0,
        extra_height=0,
        width=18,
        length=18,
        x_offset=0,
        rotation=0
    )

    thumb_row1_cap = caps.ThumbRow1Cap(
        c,
        radius=termc_config.thumb_row1_circle_radius,
        upper_length=10,
        lower_length=12,
        angle_left=8.5,
        angle_right=8.5,
        interface=c.cap_interface
    )

    thumb_row2_cap = caps.ThumbRow2Cap(
        c,
        from_angle=-17,
        to_angle=17,
        interface=c.cap_interface
    )

    r("finger", finger_cap())
    r("test_finger_half", finger_cap() - translate((20, 0, 0))(cube((40, 40, 40), center=True)))
    r("thumb_upper", thumb_row1_cap())
    r("test_thumb_upper_half", thumb_row1_cap() - translate((20, 0, 0))(cube((40, 40, 40), center=True)))
    r("thumb_lower", thumb_row2_cap())
